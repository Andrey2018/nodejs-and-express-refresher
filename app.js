const express = require('express');

const placesRoutes = require('./routes/places-routes');

const app = express();

app.use(placesRoutes);

// app.use(express.urlencoded({ extended: false }));

// app.post('/user', (req, res, next) => {
//   res.send(`<h1>${req.body.username}</h1>`);
// });

// app.get('/', (req, res, next) => {
//   res.send('<form action="/user" method="POST"><input type="text" name="username"> <button type="submit">Create User</button></form>');
// });

app.listen(5000);